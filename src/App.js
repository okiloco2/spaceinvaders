(function(scope){

	function App(){
		this.initialize();
	}
	var canvas = App.prototype;
	var stage = App.prototype;
	var background = App.prototype;

	var loader = App.prototype;
	
	var SOUNDS = App.prototype;
	var sonidosCargados = App.prototype;

	
	var background_url = App.prototype;
	var contenedorMalos = App.prototype;
	var nave_url = App.prototype;
	var rutaMalo = App.prototype;
	var start = App.prototype;
	var disparo = App.prototype;
	var gameover = App.prototype;
	
	App.prototype.initialize = function(){
		var self = this;
		this.canvas = document.createElement("canvas");
		this.canvas.width = 600;
		this.canvas.height = 600;

		this.start = false;

		var container = document.getElementById("game");
		container.appendChild(this.canvas);

		this.background_url = "src/assets/background.png";
		this.nave_url = "src/assets/nave.png";
		this.rutaMalo = "src/assets/enemy.png";
		this.rutaDisparoNave = "src/assets/bullet.png";
		var images = [
			this.background_url,
			this.nave_url,
			this.rutaMalo,
			this.rutaDisparoNave
		];


		this.stage = new createjs.Stage(this.canvas);
		this.loader = new Loader();
		this.loader.loadImages(images);
		this.loader.onComplete = function(){
			self.assetsLoades();
		}

		this.gameover = false;

	};
	App.prototype.loadSounds = function(){
		sonidosCargados = 0;
		var registerPlugins = createjs.Sound.registerPlugins([
			createjs.CocoonJSAudioPlugin,
			createjs.WebAudioPlugin,
			createjs.HTMLAudioPlugin
		]);

		if(registerPlugins){
			createjs.Sound.on("fileload",createjs.proxy(this.soundsLoaded,this));
			SOUNDS.SOUNDS = {};
			SOUNDS.SOUNDS.FLY = `src/sfx/fly.m4a`;
			createjs.Sound.registerSound(SOUNDS.SOUNDS.FLY,"FLY",4);

			SOUNDS.SOUNDS.POP = `src/sfx/bullet-01.m4a`;
			createjs.Sound.registerSound(SOUNDS.SOUNDS.POP,"POP",4);

			SOUNDS.SOUNDS.BOOM = `src/sfx/boom.m4a`;
			createjs.Sound.registerSound(SOUNDS.SOUNDS.BOOM,"BOOM",4);
		}
	}	
	App.prototype.soundsLoaded = function(e){
		sonidosCargados ++;
		if(sonidosCargados == 1){
			console.log("Sonidos Cargados!");
			this.loadImages();//Cargar Imagenes
		}
	}
	
	App.prototype.playPop = function(){
		var _pop = createjs.Sound.createInstance("POP");
		_pop.volume = .06;
		_pop.play();
	}
	App.prototype.playFire = function(){
		var _fire = createjs.Sound.createInstance("FLY");
		_fire.volume = .01;
		_fire.play();
	}
	App.prototype.playBoom = function(){
		var _boom = createjs.Sound.createInstance("BOOM");
		_boom.volume = .05;
		_boom.play();
	}	
	App.prototype.assetsLoades = function(){
		this.loadSounds();
	}

	App.prototype.loadImages = function(){
		
		var self = this;
		console.log("Imágenes cargadas: ",this.loader);

		//Agregar Fondo
		var bmp = this.loader[this.background_url];
		this.background = new createjs.Bitmap(bmp);
		this.background.alpha = .8;
		this.stage.addChild(this.background);

		createjs.Ticker.framerate = 30;
		createjs.Ticker.on("tick",function(e){
			if(!e.paused){
				self.tick();
			}
		});
		
		
		//Agregar Nave
		var dataNave = {
			images:[this.loader[this.nave_url]],
			frames:[
				[225,30,86,144],
				[324,31,86,144],
				[33,32,86,144],
				[129,32,86,144],
				[425,32,86,144],
				[524,32,86,144],
				[620,34,86,144],
				[717,35,86,144],
				[373,200,30,40],
				[669,200,30,40],
				[157,201,29,39],
				[200,201,29,39],
				[287,201,29,39],
				[329,201,29,39],
				[414,201,30,40],
				[455,201,30,40],
				[497,201,30,40],
				[540,201,30,40],
				[625,201,30,40],
				[116,203,29,39],
				[582,203,30,40],
				[75,204,29,39],
				[243,204,29,39],
				[34,205,29,39],
				[750,230,3,9],
				[754,230,3,9],
				[733,231,3,6],
				[737,231,3,6],
				[325,275,86,127],
				[425,276,86,127],
				[523,276,86,127],
				[623,276,86,127],
				[721,276,86,127],
				[128,277,86,127],
				[226,277,86,127],
				[29,278,86,127],
				[125,445,96,127],
				[29,447,86,127],
				[232,451,96,124],
				[339,459,95,117],
				[448,468,93,104],
				[553,469,57,102],
				[620,476,51,95],
				[683,482,43,88],
				[738,488,36,78],
				[446,507,2,9],
				[434,512,1,15],
				[773,550,2,4],
				[342,586,465,163],
				[101,630,67,80],
				[177,631,67,80],
				[284,631,40,44],
				[281,633,2,1],
				[278,634,2,1],
				[31,635,54,76],
				[274,635,3,3],
				[272,638,1,1],
				[265,646,1,1],
				[262,650,1,1],
				[261,653,1,1],
				[259,655,2,4],
				[258,660,1,3],
				[257,664,1,3],
				[257,676,25,34],
				[322,680,1,4],
				[320,685,2,3],
				[319,689,1,1],
				[318,691,1,1],
				[317,694,1,1],
				[311,701,1,1],
				[308,704,1,1],
				[306,705,1,1],
				[299,707,5,3],
				[284,710,10,1],
				[35,741,74,112],
				[119,753,60,96],
				[189,757,50,88],
				[354,765,67,80],
				[438,765,67,80],
				[520,767,58,68],
				[601,767,64,79],
				[250,769,39,78],
				[679,772,62,67],
				[301,775,35,72],
				[578,782,1,2],
				[581,786,1,1],
				[582,789,1,1],
				[583,791,2,4],
				[585,796,1,3],
				[600,798,1,4],
				[586,800,1,3],
				[562,812,25,34],
				[665,820,1,1],
				[532,837,1,1],
				[337,839,1,1],
				[535,840,1,1],
				[537,841,1,1],
				[540,843,5,3],
				[550,846,10,1],
				[530,872,78,80],
				[619,872,80,80],
				[441,873,78,80],
				[355,874,76,80],
				[269,876,76,80],
				[186,880,74,76],
				[32,885,67,71],
				[108,885,69,70],
				[698,897,1,1],
				[607,901,1,2],
				[193,985,5,2],
				[293,985,2,1],
				[45,986,55,22],
				[152,986,8,3],
				[250,986,9,3],
				[349,987,3,1],
				[344,988,3,2],
				[143,992,4,9],
				[244,993,1,1],
				[162,994,23,9],
				[337,994,1,1],
				[242,995,2,4],
				[112,997,1,1],
				[261,997,23,7],
				[335,997,2,3],
				[363,999,12,3],
				[359,1000,3,2],
				[242,1002,1,1],
				[376,1003,1,1],
				[355,1004,1,1],
				[205,1005,2,2],
				[62,1008,1,1],
				[104,1011,10,22],
				[50,1012,1,1],
				[59,1013,1,1],
				[41,1014,4,4],
				[53,1014,5,5],
				[152,1014,2,2],
				[202,1014,2,2],
				[250,1014,2,2],
				[48,1015,4,5],
				[401,1016,2,2],
				[342,1017,1,1],
				[38,1019,4,9],
				[45,1019,1,4],
				[103,1022,1,2],
				[137,1022,1,2],
				[210,1023,2,3],
				[235,1023,1,2],
				[139,1024,1,1],
				[237,1025,1,1],
				[99,1027,2,5],
				[205,1029,1,1],
				[56,1031,38,20],
				[155,1032,19,16],
				[256,1033,15,13],
				[111,1035,2,4],
				[349,1036,2,6],
				[37,1037,1,1],
				[105,1037,3,3],
				[102,1039,1,1],
				[204,1039,1,1],
				[270,1039,1,1],
				[35,1040,2,7],
				[111,1040,1,2],
				[134,1040,1,5],
				[100,1041,2,3],
				[197,1041,7,6],
				[232,1041,1,1],
				[271,1041,1,1],
				[106,1042,1,2],
				[304,1042,1,2],
				[45,1043,14,14],
				[232,1043,1,3],
				[350,1043,6,5],
				[93,1044,2,2],
				[99,1044,2,3],
				[143,1044,1,3],
				[355,1044,1,1],
				[241,1045,1,1],
				[300,1045,3,3],
				[399,1045,1,1],
				[361,1046,4,2],
				[95,1047,4,2],
				[98,1047,14,13],
				[241,1047,1,1],
				[335,1047,1,1],
				[397,1048,1,1],
				[144,1049,1,1],
				[187,1049,1,1],
				[296,1049,1,1],
				[335,1049,1,1],
				[183,1050,3,1],
				[210,1050,1,1],
				[242,1050,1,1],
				[395,1050,1,1],
				[146,1051,7,5],
				[208,1052,2,2],
				[336,1052,1,1],
				[245,1053,3,3],
				[61,1055,2,1],
				[154,1055,1,1],
				[206,1055,1,1],
				[204,1056,1,1],
				[307,1056,1,1],
				[338,1056,1,1],
				[42,1057,8,7],
				[68,1057,4,1],
				[250,1057,2,1],
				[253,1057,1,1],
				[66,1058,1,1],
				[201,1058,1,1],
				[305,1058,1,1],
				[340,1058,1,1],
				[62,1059,2,1],
				[141,1059,4,3],
				[197,1059,2,1],
				[301,1060,1,1],
				[343,1060,2,1],
				[65,1061,1,1],
				[239,1061,1,1],
				[296,1062,1,1],
				[64,1063,6,1],
				[165,1063,2,1],
				[241,1063,1,1],
				[399,1064,1,1]
				],
			animations:{"normal":[0,7],"boom":[70,80],"fire":[28,34],"gameover":[48,48]}
		}
		var spriteSheet = new createjs.SpriteSheet(dataNave);
		this.nave = new Nave(spriteSheet);
		this.stage.addChild(this.nave);

		this.stage.on("stagemousedown",function(e){
			self.handleMouseDown(e);
		});

		createjs.Ticker.addEventListener("tick",this.tickCrearMalos)
	}

	App.prototype.tickCrearMalos = function(){
		var app = window.app;
		var punto = app.nave.localToGlobal(app.nave.x,app.nave.y);
		if(punto.y > 200 && !this.start){
			var contenedor = new ContenedorMalos();
			app.stage.addChild(contenedor);
			contenedor.x = 0;
			contenedor.y = 0;
			app.contenedorMalos = contenedor;
			this.contenedorMalos = contenedor;
			createjs.Ticker.removeEventListener("tick",this.tickCrearMalos);

			this.start = true;
		}

	}
	App.prototype.handleMouseDown = function(e){
		this.nave.salta(e);
	}
	App.prototype.tick = function(){
		if(this.contenedorMalos && !this.nave.explotado){
			this.contenedorMalos.hitTestDisparoNave(this.disparo);
			this.contenedorMalos.hitTestChoqueNave(this.nave);
		}
		this.stage.update();
	}
	App.prototype.disparoNave = function(posX,posY){

		if(typeof(this.disparo) == "undefined" || this.disparo==null){
			this.disparo = new DisparoNave(this.loader[this.rutaDisparoNave],posX,posY);
		}
	}
	App.prototype.fin = function(){
		if(!this.gameover){
			console.log("Game Over!");
			this.gameover = true;
		}
	}
	scope.App = App;

}(window));

window.onload = function(){
	this.app = new App();
}