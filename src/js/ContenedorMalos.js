(function(scope){
	"use strict";
	function ContenedorMalos(){
		this.initialize();
	}
	//var variable = ContenedorMalos.prototype;

	ContenedorMalos.prototype = new createjs.Container();
	ContenedorMalos.prototype.Container_init = ContenedorMalos.prototype.initialize;

	var malos  = ContenedorMalos.prototype;
	var indiceMalos  = ContenedorMalos.prototype;
	var topeMalos  = ContenedorMalos.prototype;
	var filas  = ContenedorMalos.prototype;
	var pasos  = ContenedorMalos.prototype;
	var initY  = ContenedorMalos.prototype;
	var cargado  = ContenedorMalos.prototype;
	

	var vX  = ContenedorMalos.prototype;
	var vY  = ContenedorMalos.prototype;

	const MAX_STEPS = 2;
	const MEX_HEIGHT = 550;

	ContenedorMalos.prototype.initialize = function(spriteSheet){

		this.Container_init();

		this.vX = 6;
		this.vY = 70;
		this.velocity = {x:this.vX,y:0};
		this.init();
	};
	ContenedorMalos.prototype.creaMalo = function(){
		if(!this.cargado){
			if(this.indiceMalos < this.topeMalos * this.filas){
				var dataMalo = {
					images:[window.app.loader[window.app.rutaMalo]],
					frames:[
						[70,0,37,32],
						[3,47,37,32],
						[46,48,38,34],
						[91,50,41,33],
						[142,50,12,15],
						[160,50,13,25],
						[142,67,24,18],
						[2,106,37,32],
						[44,106,37,32],
						[95,106,37,32],
						[136,106,37,32],
						[0,156,1,1],
						[48,156,1,1],
						[90,156,1,1],
						[138,156,1,1],
						[0,158,37,36],
						[48,158,37,36],
						[90,158,37,36],
						[138,158,37,36]
					],
					animations:{run:[7,10],boom:[0,4]}
				};

				var posY = Math.floor(this.indiceMalos / this.topeMalos);
				var posX = this.indiceMalos - (posY * this.topeMalos);

				var spriteSheet = new createjs.SpriteSheet(dataMalo);
				var malo = new Malo(spriteSheet);
				malo.x = 50 + posX*80;
				malo.y = this.initY + posY *60;
				this.addChild(malo);

				this.malos.push(malo);
				this.indiceMalos ++;
			}else{
				this.cargado = true;
				createjs.Ticker.removeEventListener("tick",this.tickCreador);
			}
		}
	}
	ContenedorMalos.prototype.init = function(){
		var self = this;
		this.cargado = false;

		console.log("Iniciando Contendor Malos.");

		this.x = 0;
		this.y = 0;

		this.malos = new Array();
		this.indiceMalos = 0;
		this.topeMalos = 7;
		this.filas = 3;
		this.pasos = 0;
		this.initY = 60;

		this.snapToPixel = true;

		createjs.Ticker.addEventListener("tick",this.tickCreador);

		createjs.Ticker.addEventListener("tick", function(e){
			if(!window.app.gameover)
			self.onTick();
		});

	}
	ContenedorMalos.prototype.onTick = function(){
		this.x += this.velocity.x;
		this.y += this.velocity.y;

		if(this.x >= 200){
			this.velocity.x = this.velocity.x * -1;
		}else if(this.x <= -200){
			this.velocity.x = this.velocity.x * -1;
			if(this.pasos < MAX_STEPS){
				this.pasos++;
			}else{
				this.pasos = 0;
				this.y += this.vY;
				if(this.y > MEX_HEIGHT){
					this.y = 0;
				}
			}
		}
	}

	ContenedorMalos.prototype.tickCreador = function(){
		 window.app.contenedorMalos.creaMalo();
		 //this.creaMalo();
	}

	ContenedorMalos.prototype.hitTestChoqueNave = function(nave){
		if(nave != null && !window.app.nave.explotado){
			this.malos.forEach((malo,i) => {
			  var col = ndgmr.checkRectCollision(malo,nave);
			  if(col){
			  	  malo.explota();
			  	  this.malos.splice(i,1);
				  nave.explota();
				  return;
			  }
			})
		}
	}	
	ContenedorMalos.prototype.hitTestDisparoNave = function(disparo){
		if(disparo!=null && !window.app.nave.explotado){
			for(var i = 0; i<this.malos.length;++i){
			  var malo = this.malos[i];
			  var col = ndgmr.checkRectCollision(malo,disparo);
			  if(col){
			  	malo.explota();
			  	this.malos.splice(i,1);
			  	disparo.destroy();
			  	this.testTotales();
			  	return;
			  }
			};
		}
	}
	ContenedorMalos.prototype.testTotales = function(){
		if(this.malos.length == 0){
			this.init();
			this.acelera();
		}
	}
	ContenedorMalos.prototype.acelera = function(){
		this.velocity.x += 2;
	}
	scope.ContenedorMalos = ContenedorMalos;

}(window));
