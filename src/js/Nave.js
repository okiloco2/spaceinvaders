(function(scope){

	function Nave(spriteSheet){
		this.initialize(spriteSheet);
	}
	//var variable = Nave.prototype;

	const MAX_Y = 400;
	const LAST_KEY_FRAME = 80;

	Nave.prototype = new createjs.Sprite();
	Nave.prototype.BitmapAnimation_init = Nave.prototype.initialize;

	var width  = Nave.prototype;
	var height  = Nave.prototype;
	var explotado = Nave.prototype;
	var velocity = Nave.prototype;
	


	Nave.prototype.initialize = function(spriteSheet){
		this.BitmapAnimation_init(spriteSheet);

		var self = this;

		this.gotoAndPlay("normal");

		this.x = 256;
		this.y = 100;
		this.ancho = spriteSheet.getFrame(0).rect.width;
		this.alto = spriteSheet.getFrame(0).rect.height;

		this.snapToPixel = true;
		this.velocity = {x:0,y:-15};

		createjs.Ticker.on("tick", function(e){
			self.onTick();
		});

		this.addEventListener("mousedown",(e) => {
			self.handleMouseDown(e);
		});

		this.explotado = false;
	};
	Nave.prototype.handleMouseDown = function(e){
		if(!this.explotado){
			this.dispara();
		}
	}
	Nave.prototype.dispara = function(e){
		this.velocity.x = 0;
		this.velocity.y = 0;

		this.gotoAndPlay("normal");

		window.app.disparoNave(this.x + this.ancho/2 - 8,this.y);
	}
	Nave.prototype.salta = function(e){

		//if(this.explotado) return;
		if(!this.explotado){
			if((e.stageX >= this.x) && (e.stageX < this.x + this.ancho)){

			}else if(e.stageX > this.x){
				window.app.playFire();
				this.velocity.x += 10;
			}else if(e.stageX < this.x){
				window.app.playFire();
				this.velocity.x -= 10;
			}

			if(e.stageY <= this.y - 20){
				window.app.playFire();
				this.velocity.y = -15;
			}else if(e.stageY > this.y - 20 && e.stageY < this.y + this.alto){
				this.velocity.y = +0;
			}else{
				window.app.playFire();
				this.velocity.y = +15;
			}
			this.gotoAndPlay("fire");
		}
	}
	Nave.prototype.onTick = function(){

		//
		this.velocity.y +=1;
		if(this.velocity.y < 0 || this.y < MAX_Y){
			if(!this.explotado)
			this.y += this.velocity.y;
		}
		if(this.velocity.x != 0){
			if(this.velocity.x > 0){
				this.velocity.x -=1;
			}else{
				this.velocity.x +=1;
			}
			this.x += this.velocity.x;
		}
		if(this.currentFrame == 34){
			this.gotoAndPlay("normal");
		}else if(this.currentFrame == LAST_KEY_FRAME){//Eliminar Nave
			if(!window.app.gameover){
				console.log("> ",this.currentFrame,LAST_KEY_FRAME,(this.currentFrame == LAST_KEY_FRAME));
				window.app.fin();
				this.parent.removeChild(this);
				//this.gotoAndPlay("gameover");
				this.x = 55;
				this.y = 100;
			}
		}
	}
	Nave.prototype.explota = function(e){

		if(!this.explotado){
			console.log("Nave Explota!");
			window.app.playBoom();
			this.explotado = true;
			this.gotoAndPlay("boom");
		}
	}
	scope.Nave = Nave;

}(window));
