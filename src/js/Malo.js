(function(scope){

	function Malo(spriteSheet){
		this.initialize(spriteSheet);
	}
	//var variable = Malo.prototype;

	const MAX_Y = 400;
	const LAST_KEY_FRAME = 4;

	Malo.prototype = new createjs.Sprite();
	Malo.prototype.BitmapAnimation_init = Malo.prototype.initialize;

	var ancho  = Malo.prototype;
	var alto  = Malo.prototype;

	Malo.prototype.initialize = function(spriteSheet){
		this.BitmapAnimation_init(spriteSheet);

		var self = this;

		this.gotoAndPlay("run");

		this.ancho = spriteSheet.getFrame(0).rect.width;
		this.alto = spriteSheet.getFrame(0).rect.height;

		this.snapToPixel = true;

		createjs.Ticker.on("tick", function(e){
			self.onTick();
		});
	};
	Malo.prototype.onTick = function(){
		if(this.currentFrame == LAST_KEY_FRAME){//KEY_Eliminar el Malo
			if(this.parent){
				this.parent.removeChild(this);
			}
		}
	}
	Malo.prototype.explota = function(){
		window.app.playBoom();
		this.gotoAndPlay("boom");
	}
	scope.Malo = Malo;

}(window));
