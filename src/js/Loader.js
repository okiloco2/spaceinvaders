(function(scope){

	function Loader(){
		this.initialize();
	}
	
	var cargadas = Loader.prototype;
	var total = Loader.prototype;
	var onComplete = Loader.prototype;

	Loader.prototype.initialize = function(){
		console.log("Loader initialized!");
	};

	Loader.prototype.loadImages = function(images){
		this.cargadas = 0;
		this.total = images.length;

		images.forEach((ruta) => {
		  this.loadImage(ruta);
		});
	}
	Loader.prototype.loadImage = function(ruta){
		var self = this;
		var image = new Image();
		this[ruta] = image;
		image.onload = function(e){
			self.imageLoaded();
		}
		image.src = image.url = ruta;
	}
	Loader.prototype.imageLoaded = function(){
		this.cargadas ++;
		if(this.cargadas === this.total){
			if(this.onComplete){
				this.onComplete();
			}else{
				console.log("No se ha definido onComplete");
			}
		}
	}

	scope.Loader = Loader;

}(window));