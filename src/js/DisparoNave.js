(function(scope){

	function DisparoNave(image,posX,posY){
		this.initialize(image,posX,posY);
	}
	//var variable = DisparoNave.prototype;

	DisparoNave.prototype = new createjs.Bitmap();
	DisparoNave.prototype.Bitmap_init = DisparoNave.prototype.initialize;

	var ancho  = DisparoNave.prototype;
	var alto  = DisparoNave.prototype;
	
	var destroyed = DisparoNave.prototype;

	DisparoNave.prototype.initialize = function(image,posX,posY){
		
		var self = this;
		this.ancho = 50;

		this.Bitmap_init(image);

		this.x = posX;
		this.y = posY;

		app.stage.addChild(this);

		this.snapToPixel = true;
		this.velocity = {x:0,y:-15};

		createjs.Ticker.on("tick", function(e){
			self.onTick();
		});

		this.destroyed = false;

		console.log("Disparo nave ready!");
		window.app.playPop();
	};
	DisparoNave.prototype.handleMouseDown = function(e){
		if(!this.explotado){
			this.dispara();
		}
	}
	DisparoNave.prototype.dispara = function(e){
		this.velocity.x = 0;
		this.velocity.y = 0;

		this.gotoAndPlay("normal");

		window.app.disparoDisparoNave(this.x + this.ancho/2 - 25,this.y);
	}
	DisparoNave.prototype.onTick = function(){
		if(this.y <= 0){
			this.destroy();
		}else if(this.velocity.y < 0){
			this.y += this.velocity.y;
		}
	}
	DisparoNave.prototype.destroy = function(){
		if(!this.destroyed){
			console.log("Disparo Eliminado!")
			window.app.disparo = null;
			app.stage.removeChild(this);
			this.destroyed = true;
		}
	}
	scope.DisparoNave = DisparoNave;

}(window));
